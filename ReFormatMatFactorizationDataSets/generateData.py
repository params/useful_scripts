import sys

#This script is used to compress the datasets for matrix factorization
#removing unused users, items

def main(args):
    #print "Args: " + str(args)
    f_users = args[1]
    f_items = args[2]
    f_triplets = args[3]
    f_output = args[4]

    items = {}
    users = {}

    #print "Reading users file: " + f_users
    lines = open(f_users, "r")
    count = 1
    for line in lines:
        users[line.strip()] = count
        count = count + 1
    lines.close()
    print "Added to users: " + str(len(users))

    #print "Reading items file: " + f_items
    lines = open(f_items, "r")
    count = 1
    for line in lines:
        items[line.strip()] = count
        count = count + 1
    lines.close()
    print "Added to items: " + str(len(items))

    print "Reading triplets file: " + f_triplets
    fw = open(f_output, "w")
    lines = open(f_triplets, "r")
    for line in lines:
        tokens = line.strip().split(" ")
        output = ""
        if tokens[0] in users:
            output += str(users[tokens[0]]) + " "
        else:
            print "not found"
        if tokens[1] in items:
            output += str(items[tokens[1]]) + " " + tokens[2]
        else:
            print "not found"
        print output
        fw.write(output + "\n")
    lines.close()
    fw.close();

if __name__ == "__main__":
    args = sys.argv
    if len(args) < 4:
        print "Pass the following args: <users_file> <items_file> <triplets_file> <output_file>"
        sys.exit(0)
    main(args)
