import os
import subprocess

queue_name = "ml"

datasets = [
		("letor_ohsumed", 
		 "/group/ml/data/mc/letor-3.0/letor_ohsumed/Fold1/train2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_ohsumed/Fold1/test2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_ohsumed/Fold1/vali2.txt",
		 "/group/ml/data/mc/letor-3.0/letor_ohsumed/Fold1/ohsumed_model.txt",
		 "skip"
                ),
		("letor_2003_hp", 
		"/group/ml/data/mc/letor-3.0/letor_2003_hp/Fold1/train2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_2003_hp/Fold1/test2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_2003_hp/Fold1/vali2.txt",
		 "/group/ml/data/mc/letor-3.0/letor_2003_hp/Fold1/2003_hp_model.txt",
		 "noskip"
                ), 
		("letor_2004_hp", 
		 "/group/ml/data/mc/letor-3.0/letor_2004_hp/Fold1/train2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_2004_hp/Fold1/test2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_2004_hp/Fold1/vali2.txt",
		 "/group/ml/data/mc/letor-3.0/letor_2004_hp/Fold1/2004_hp_model.txt",
		 "skip"
		),
		("letor_2003_td", 
		 "/group/ml/data/mc/letor-3.0/letor_2003_td/Fold1/train2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_2003_td/Fold1/test2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_2003_td/Fold1/vali2.txt",
		 "/group/ml/data/mc/letor-3.0/letor_2003_td/Fold1/2003_td_model.txt",
		 "skip" 
		),
		("letor_2004_td", 
		 "/group/ml/data/mc/letor-3.0/letor_2004_td/Fold1/train2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_2004_td/Fold1/test2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_2004_td/Fold1/vali2.txt",
		 "/group/ml/data/mc/letor-3.0/letor_2004_td/Fold1/2004_td_model.txt",
		 "skip"
		),
		("letor_ltrc", 
		 "/group/ml/data/mc/letor-3.0/letor_ltrc/set1.train2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_ltrc/set1.test2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_ltrc/set1.vali2.txt", 
		 "/group/ml/data/mc/letor-3.0/letor_ltrc/ltrc_model.txt", 
		 "noskip"
		)
]

truncations = [10, 5, 3, 1]
reg = [0.01]
numnodes = 1
numcpus = 1
tol = 0.001

subs_dir = "auto_subs"
#logs_dir = "auto_logs_zero_w"
logs_dir = "auto_logs_bmrm_w"

for dataset, train, test, valid, model, skip_flag in datasets:
    if skip_flag == "skip":
	continue
    for trunc in truncations:
	for regul in reg:
		task_name = "D%s_NDCG@%d_L%f" % (dataset, trunc, regul)
		
		if not os.path.exists(subs_dir):
			os.makedirs(subs_dir)
		if not os.path.exists(logs_dir):
			os.makedirs(logs_dir)
		sub_fname = subs_dir + "/%s.sub" % (task_name)
		log_fname = logs_dir + "/%s.log" % (task_name)
		prog_file = os.getcwd() + "/" + logs_dir + "/%s.progress" % (task_name)
		
		ofile = open(sub_fname, "w")
		
		ofile.write("#!/bin/sh -l \n")
		ofile.write("# FILENAME: %s \n" % (sub_fname));
		ofile.write("#PBS -l nodes=%d:ppn=%d \n" % (numnodes, numcpus))
		ofile.write("#PBS -l walltime=120:00:00 \n")
		ofile.write("#PBS -l mem=20gb \n")
		ofile.write("#PBS -N %s \n" % task_name)
		ofile.write("#PBS -q %s \n" % queue_name)
		ofile.write("#PBS -e " + logs_dir + "/%s.err \n" % task_name)
		ofile.write("#PBS -o " + logs_dir + "/%s.out \n" % task_name)

		ofile.write("module load devel\n")
		ofile.write("module load intel/13.0.1.117\n")
		#ofile.write("module load glib/2.21.3\n");
		#ofile.write("module load mpich2/1.4.1p1_intel-13.0.1.117\n")
		ofile.write("module load petsc/3.4.2_impi-4.1.0.030_intel-13.1.1.163\n")
		ofile.write("module load python/2.7.5_intel-13.1.1.163\n")

		#ofile.write("./tsolver/ranker -train %s -test %s -trunc %d -lambda %f -tol %f > %s\n" % (train, test, trunc, regul, tol, prog_file));
		ofile.write("./tsolver/ranker -train %s -test %s -trunc %d -lambda %f -tol %f -w_file %s > %s\n" % (train, test, trunc, regul, tol, model, prog_file));
		ofile.write("\n")
		ofile.close()
		
		qsub_command = "qsub %s" % sub_fname
		print sub_fname + " submitted to " + queue_name
			
		p = subprocess.Popen(qsub_command, shell=True)
		p.communicate()
        
    
