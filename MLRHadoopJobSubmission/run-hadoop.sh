#! /bin/bash

#Verify if arguments are passed correctly
if [ "$#" -ne 1 ] || [ "$1" -gt 2 ] || [ "$1" -lt 0 ]; then
  echo "Usage: ./run-hadoop.sh <0|1|2>" >&2
  echo "argument = 0 => Run entire script (default)" >&2
  echo "argument = 1 => Run converter job only" >&2
  echo "argument = 2 => Run pMLR job only" >&2
  exit 1
fi

#Data directory (Assumption: All required data is copied to this dir).
DATA_DIR=/data/ml

if [ "$1" -eq 0 ] || [ "$1" -eq 1 ]; then
	echo "Running converter job"
	
	#Run Converter hadoop job
	hadoop fs -rmr -skipTrash input
	hadoop fs -rmr -skipTrash output
	hadoop fs -mkdir input/trainfile
	hadoop fs -put $DATA_DIR/clef.train.normalized.svm input/trainfile
	hadoop fs -ls input/trainfile
	hadoop jar $DATA_DIR/MulticlassClassifier.jar org.pMLR.hadoop.Converter \
		-D gc.Converter.input=input/trainfile \
		-D gc.Converter.output=output/seqfile/train \
		-D gc.Converter.name=converter
fi


if [ "$1" -eq 0 ] || [ "$1" -eq 2 ]; then
	echo "Running pMLR job"
	
	#Run pMLR hadoop job
	hadoop fs -rmr -skipTrash input/leaflabels
	hadoop fs -rmr -skipTrash itermlr
	hadoop fs -mkdir input/leaflabels
	hadoop fs -put /data/ml/clef.train.labels input/leaflabels
	hadoop jar /data/ml/pMLR.jar org.pMLR.hadoop.TrainingDriver \
		-D gc.iterativemlr-train.startiter=0 \
		-D gc.iterativemlr-train.lambda=.001 \
		-D gc.iterativemlr-train.split=20 \
		-D mapred.job.map.memory.mb=1000 \
		-D mapred.job.reduce.memory.mb=1000 \
		-D mapred.child.java.opts=-Xmx1000m \
		-D mapred.task.timeout=0 \
		-D mapred.map.max.attempts=100 \
		-D gc.iterativemlr-train.iterations=100 \
		-D gc.iterativemlr-train.maxiter=200 \
		-D gc.iterativemlr-train.eps=.0001 \
		-D gc.dataset.train.loc=output/seqfile/train/ \
		-D gc.iterativemlr-train.input=input/leaflabels/* \
		-D gc.iterativemlr-train.output=itermlr/weights/ \
		-D gc.iterativemlr-train-vparam.output=itermlr/vparams/ \
		-D gc.iterativemlr-train-fvalues.dir=itermlr/fvalues/ 
fi
echo "Script terminating.."
