#! /bin/bash

#Script to set files and directories with standard (good) permissions
#To be run in the directory where files and directories need to be affected

find "$PWD" -user $USER -exec chgrp ml-data {} \;
find "$PWD" -user $USER -type d -exec chmod 775 {} \;
find "$PWD" -user $USER -type f -exec chmod 664 {} \;

#Uncomment the line below to verify updated permissions
#find "$PWD" -user $USER -exec stat -c '%a %n' {} \; 
